# Variables to set

#  directory we need to build project
SET(EO_DIR "/home/dell/Documents/ADDCproject/Software/GEplayground/GEsystem/SearchEngines/EO/EO-1.3.1" CACHE PATH "EO directory" FORCE)

# automagically set parameters, do not edit

SET(EO_INCLUDE_DIRS "${EO_DIR}/src" CACHE PATH "EO include directory" FORCE)
SET(EO_LIBRARY_DIRS "${EO_DIR}/release/lib" CACHE PATH "EO library directory" FORCE)
SET(EO_LIBRARIES eoutils eo es ga cma gcov) # do not use quotes around this list or it will fail
