# CMake generated Testfile for 
# Source directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo
# Build directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("doc")
subdirs("src")
subdirs("test")
subdirs("tutorial")
