# Install script for directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibrariesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/lib/libeo.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xheadersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eo" TYPE FILE FILES
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/EO.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/PO.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/apply.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoAlgo.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoBinaryFlight.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoBitParticle.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoBreed.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoCellularEasyEA.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoCloneOps.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoCombinedContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoCombinedInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoConstrictedVariableWeightVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoConstrictedVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoCounter.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoCtrlCContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoDetSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoDetTournamentSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoDistribUpdater.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoDistribution.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoDualFitness.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEDA.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEasyEA.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEasyPSO.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalCounterThrowException.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalFunc.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalFuncCounter.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalFuncCounterBounder.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalFuncPtr.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalTimeThrowException.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoEvalUserTimeThrowException.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoExceptions.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoExtendedVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFactory.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFitContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFitnessScalingSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFixedInertiaWeightedVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFlOrBinOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFlOrMonOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFlOrQuadOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFlight.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFunctor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoFunctorStore.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoG3Replacement.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoGaussRealWeightUp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoGenContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoGenOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoGeneralBreeder.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoInitializer.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoInt.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoIntegerVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoInvalidateOps.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoLinearDecreasingWeightUp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoLinearFitScaling.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoLinearTopology.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoMGGReplacement.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoMerge.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoMergeReduce.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoNDSorting.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoNeighborhood.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoObject.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoOneToOneBreeder.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoOpContainer.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoOpSelMason.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoOrderXover.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPSO.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoParticleBestInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoParticleFullInitializer.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPerf2Worth.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPeriodicContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPersistent.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPop.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPopEvalFunc.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPopulator.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPrintable.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoPropGAGenOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoProportionalCombinedOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoProportionalSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRandomRealWeightUp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRandomSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRankMuSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRanking.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRankingSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRealBoundModifier.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRealParticle.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoReduce.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoReduceMerge.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoReduceMergeReduce.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoReduceSplit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoReplacement.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoRingTopology.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSGA.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSGAGenOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSGATransform.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSIGContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSTLFunctor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoScalarFitness.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoScalarFitnessAssembled.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSecondsElapsedContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelectFactory.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelectFromWorth.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelectMany.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelectNumber.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelectOne.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSelectPerc.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSequentialSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSharing.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSharingSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoShiftMutation.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSigBinaryFlight.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSimpleEDA.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSocialNeighborhood.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoStandardFlight.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoStandardVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoStarTopology.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSteadyFitContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoStochTournamentSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoStochasticUniversalSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSurviveAndDie.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSwapMutation.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoSyncEasyPSO.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTimeContinue.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTopology.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTransform.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTruncSelect.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTruncatedSelectMany.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTruncatedSelectOne.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoTwoOptMutation.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVariableInertiaWeightedVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVariableLengthCrossover.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVariableLengthMutation.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVector.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVectorParticle.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVelocity.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoVelocityInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eoWeightUpdater.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/ga.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/eo"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/do/cmake_install.cmake")
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/cmake_install.cmake")
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/ga/cmake_install.cmake")
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/gp/cmake_install.cmake")
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/other/cmake_install.cmake")
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/cmake_install.cmake")
  include("/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/serial/cmake_install.cmake")

endif()

