# Install script for directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibrariesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/lib/libes.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibrariesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/lib/libcma.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xheadersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eo/es" TYPE FILE FILES
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/CMAParams.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/CMAState.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eig.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoCMABreed.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoCMAInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsChromInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsFull.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsGlobalXover.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsMutate.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsMutationInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsSimple.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsStandardXover.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoEsStdev.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoNormalMutation.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoReal.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoRealAtomXover.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoRealInitBounded.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoRealOp.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/eoSBXcross.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/make_es.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/make_genotype_real.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/make_op.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/make_op_es.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/make_op_real.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/make_real.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/es/matrices.h"
    )
endif()

