# Install script for directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xlibrariesx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/lib/libeoutils.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xheadersx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eo/utils" TYPE FILE FILES
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/compatibility.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoAssembledFitnessStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoCheckPoint.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoData.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoDistance.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoFDCStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoFeasibleRatioStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoFileMonitor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoFileSnapshot.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoFuncPtrStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoGenCounter.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoGnuplot.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoGnuplot1DMonitor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoGnuplot1DSnapshot.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoHowMany.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoIntBounds.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoLogger.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoMOFitnessStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoMonitor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoOStreamMonitor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoParallel.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoParam.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoParser.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoPopStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoRNG.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoRealBounds.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoRealVectorBounds.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoRndGenerators.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoScalarFitnessStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoSignal.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoStat.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoState.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoStdoutMonitor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoTimeCounter.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoTimedMonitor.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoTimer.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoUniformInit.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoUpdatable.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/eoUpdater.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/pipecom.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/rnd_generators.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/selectors.h"
    "/home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src/utils/checkpointing"
    )
endif()

