# CMake generated Testfile for 
# Source directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src
# Build directory: /home/henry/Documents/UniversityOfLimerick/libge/SearchEngines/EO-1.3.1/eo/src
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("do")
subdirs("es")
subdirs("ga")
subdirs("gp")
subdirs("other")
subdirs("utils")
subdirs("serial")
