// santafe-lua.cpp -*- C++ -*-

#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>

#include<ga/ga.h>
#include<GE/ge.h>
#include"GEListGenome.h"

// Use lua for evaluation
extern "C"{
  #include<lua.h>
  #include<lauxlib.h>
  #include<lualib.h> 
}

using namespace std;

// GE mapper, declared in main.cpp
extern GEGrammarSI mapper;

// Numer of Objective function calls, declared in main.cpp
extern int obj_calls;

// Fitness value;
int fitness;

// Lua state.
lua_State *L;

//Initialises the mapper, and the s-lang interpreter
void app_init(unsigned int wrappingEvents,string grammarFile){
  
	/* Init GE mapper */
	//Set maximum number of wrapping events per mapping
	mapper.setMaxWraps(wrappingEvents);
  
	//Load grammar
	if (!mapper.readBNFFile(grammarFile)){
		cerr << "Could not read grammar.bnf\n";
		cerr << "Execution aborted.\n";
		exit(0);
	}
	
  /* Set up lua and load required libraries. */
  //L = lua_open(); deprecated 
  L = luaL_newstate();
	luaL_openlibs(L);

	//Load GEant code
	if((luaL_loadfile(L,"GEant.lua"))||lua_pcall(L,0,0,0)){
		cerr << lua_tostring(L,-1) << "\n";
		cerr << "Could not read GEant.lua\n";
		cerr << "Execution aborted.\n";
		exit(0);
	}
	
	//Load GETrail code
	if((luaL_loadfile(L,"GEtrail.lua"))||lua_pcall(L,0,0,0)){
		cerr << lua_tostring(L,-1) << "\n";
		cerr << "Could not read GEtrail.lua\n";
		cerr << "Execution aborted.\n";
		exit(0);
	}
	
	//
	char const *initcode="ReadTrailGEtrail(\"santafe.trl\")";
	if(luaL_loadbuffer(L,initcode,strlen(initcode),"line")||
		lua_pcall(L,0,0,0)){
		cerr << lua_tostring(L,-1);
		cerr << "Could not execute initial trail code.\n";
		cerr << "Execution aborted.\n";
		exit(0);
	}
	
}

//Attributes a fitness score to a genome
float objfunc(GAGenome &g){
	obj_calls++;
	string buffer;
	GEListGenome &genome = static_cast<GEListGenome&>(g);
	
  //Assign genotype to mapper
	mapper.setGenotype(genome);
	
  //Grab phenotype
        Phenotype const *phenotype=mapper.getPhenotype();

	if(phenotype->getValid()){
		//Write start code to buffer
		buffer="initGEtrail()\nresetGEtrail(615)\nwhile _energy>0 do\n";
		//Write phenotype code to buffer
		buffer+=phenotype->getString();
		//Write end code to buffer
		buffer+="\nend\n";
		
    //Load and interpret buffer
		if(luaL_loadbuffer(L,buffer.c_str(),buffer.size(),"line")||
			lua_pcall(L,0,0,0)){
			// Lua error compiling buffer
			cout << lua_tostring(L,-1);
			fitness=0;
		}
		else{
			lua_getglobal(L,"_picked_up");
			fitness=static_cast<int>(lua_tonumber(L,-1));
		}
		genome.setEffectiveSize(mapper.getGenotype()->getEffectiveSize());
		return fitness;
	}
	return 0;
}

//Print an individual to stdout
void print_individual(const GAGenome &g){
	GAListGenome<unsigned char> &genome =
		(GAListGenome<unsigned char> &) g;
	//Assign genotype to mapper
	mapper.setGenotype(genome);
	//Print phenotype
	cout << *(mapper.getPhenotype());
	cout << endl;
	cout << "Genotype = " << *mapper.getGenotype() << "\n";
	cout << "Total length     = "
		<< mapper.getGenotype()->size() << "\n";
	cout << "Effective length = "
		<< mapper.getGenotype()->getEffectiveSize() << "\n";
	cout << "Wrapping events = "
		<< mapper.getGenotype()->getWraps() << "\n";
}


