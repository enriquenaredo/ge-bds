// main.cpp -*- C++ -*-

#ifndef _MAIN_CPP_
#define _MAIN_CPP_

#include<iostream>
#include<ga/ga.h>
#include<GE/ge.h>
// Genome definition
#include"GEListGenome.h"

using namespace std;

// Function prototypes
float app_init(unsigned int, string);
float objfunc(GAGenome&);
void initFuncRandom(GAGenome&);
void initFuncSI(GAGenome&);
void print_individual(const GAGenome &g);

// Static mapper for initialisation and evaluation of individuals.
GEGrammarSI mapper;

// Parameters for Random Initialisation function.

// Minimum size for Random Initialisation.
unsigned int minSize=15; 

// Maximum size for Random Initialisation.
unsigned int maxSize=25; 

// Number of Objective function calls
int obj_calls = 0;

int main(int argc, char **argv){
	// Print version of libGE used
	cout << "Using libGE version " << libGEVersion << endl;
  
	// Parse command-line for options not taken care by GAParameterList
  
  // Random seed.
	unsigned int seed=0; 
  // Grammar file.
	string grammarFile("grammar.bnf");
  // Wrapping events.
	unsigned int wrappingEvents=0; 
  // Use Sensible Initialisation.
	bool sensibleInit=false; 
  // Grow rate for Sensible Initialisation.
	float grow=0.5; 
  // Maximum tree depth for Sensible Initialisation.
	unsigned int maxDepth=10; 
  // Tail size for Sensible Initialisation.
	unsigned int tailSize=0; 
  // Tail ratio for Sensible Initialisation.
	float tailRatio = 0.0; 
  // Use Effective Crossover.
	bool effectiveXO=false; 
  // loop 
	for(int ii=1; ii<argc; ii++){
		if(strcmp(argv[ii],"seed")==0){
			seed=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"grammar")==0){
			grammarFile=argv[++ii];
		}
		else if(strcmp(argv[ii],"wrap")==0){
			wrappingEvents=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"sensible")==0){
			sensibleInit=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"min")==0){
			minSize=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"max")==0){
			maxSize=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"grow")==0){
			grow=atof(argv[++ii]);
		}
		else if(strcmp(argv[ii],"maxDepth")==0){
			maxDepth=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"tailSize")==0){
			tailSize=atoi(argv[++ii]);
		}
		else if(strcmp(argv[ii],"tailRatio")==0){
			tailRatio=atof(argv[++ii]);
		}
		else if(strcmp(argv[ii],"effective")==0){
			effectiveXO=atoi(argv[++ii]);
		}
	}

	// Register default values of parameters.
	GAParameterList params;
	GASteadyStateGA::registerDefaultParameters(params);
  // Population size.
	params.set(gaNpopulationSize,20); 
  // Number of generations.
	params.set(gaNnGenerations,10); 
  // Probability of crossover.
	params.set(gaNpCrossover,0.9); 
  // Probability of mutation.
	params.set(gaNpMutation,0.01); 
  // Replacement strategy for steady-state GA
	params.set(gaNpReplacement,1.0); 
  // How often to record scores.
	params.set(gaNscoreFrequency,1); 
  // How often to dump scores to file.
	params.set(gaNflushFrequency,1); 
  // Output data file.
	params.set(gaNscoreFilename,"sf-output.dat"); 
	//  params.read("settings.txt"); // Grab values from file first.
  // Parse command line for GAlib args.
	params.parse(argc,argv,gaFalse); 
	// Create genome, and associate the SantaFe objective function.
	//GAListGenome<unsigned char> genome(objfunc);
	GEListGenome genome;
	genome.evaluator(objfunc);
  
	// Use our initialisation function (initFuncRandom or initFuncSI).
	if(!sensibleInit){
		genome.initializer(::initFuncRandom);
	}
	else{
		genome.initializer(::initFuncSI);
	}
	// Use the one-point xo from GALib, or our effective version.
	if(!effectiveXO){
		//genome.crossover(GAListGenome<unsigned char>::OnePointCrossover);
		genome.crossover(GEListGenome::OnePointCrossover);
	}
	else{
		genome.crossover(GEListGenome::effCrossover);
	}
	//Use our point-mutation.
	genome.mutator(GEListGenome::pointMutator);

	//Initialise libGE mapper.
	app_init(wrappingEvents,grammarFile);

	// Create GA with a steady-state approach.
	GASteadyStateGA ga(genome);
	// Associate parameters.
	ga.parameters(params);

	// Mapper settings.
	mapper.setGenotypeMaxCodonValue(UCHAR_MAX);
	mapper.setPopSize(ga.populationSize());
	mapper.setGrow(grow);
	mapper.setMaxDepth(maxDepth);
	if(tailSize){
		mapper.setTailSize(tailSize);
	}
	else{
		mapper.setTailRatio(tailRatio);
	}

	// Stats settings.
	// Which scores should we track?
	ga.set(gaNselectScores,
	 	GAStatistics::AllScores);
	// Stats file.
	ofstream stats("stats");

	// Apply all settings to ga, including random seed
	ga.initialize(seed);

	//Evolutionary cycle
	while(!ga.done()){
		ga.step();
		cout << ".";
		cout.flush();
		stats << ga.statistics().generation() << "\t"
			<< obj_calls << "\t"
			<< ga.statistics().current(GAStatistics::Maximum) << "\t"
			<< ga.statistics().current(GAStatistics::Mean) << "\t"
			<< ga.statistics().current(GAStatistics::Minimum) << "\n";
	}
	cout << endl;
	//Output statistics
	cout << ga.statistics() << endl;
	//Print best individual
	cout << "Best individual:\n";
	print_individual(ga.statistics().bestIndividual());

	return 0;
}

#endif

