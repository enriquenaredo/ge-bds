//AGLookUp.cpp -*- C++ -*-
#ifndef _AGLOOKUP_CPP_
#define _AGLOOKUP_CPP_

#include <iostream>
#include <string>
#include <typeinfo> 
#include "symbolclasses.h"

using namespace std;


///////////////////////////////////////////////////////////////////////////////
// This function generates an AGSymbol object of appropriate type. Implement it 
// after finalising grammar.bnf and symbolclasses.h (and .cpp). The function is
// used when: 
// (1)- a CFGrammar (or a derived) Object is constructed for the first time. At  
// 	this time we have to parse the strings to determine the corresponding 
//	classes.
// (2)- an AGDerivationTree object is generated to map a genotype. At this time 
//	we can use RTTI of the existing objects to generate a new object of 
//	matching type. This can be made more efficient by recycling AGSymbol 
//	objects instead of creating afresh every time. 
AGSymbol* AGLookUp::findAGSymbol(const Symbol& agSym, const unsigned int productions, bool grammarMade){
#if (DEBUG_LEVEL >= 2)
        cerr << "'AGLookUp::findAGSymbol(const Symbol*, const Rule*, bool)' called\n";
#endif
	const string sym = (const string)agSym;
	AGSymbol* ret_object=NULL;
	string errormessage;
	
	if (!grammarMade){ //Generating Grammar for the first time

		if (sym.size()==3){ //<S>, <K>, <I>
			if (sym[0]=='<' && sym[2]=='>' ){
                        	switch (sym[1]){
                                	case 'S':       ret_object=new S(sym, agSym.getType());
                                       			break;
					case 'K':       ret_object=new K(sym, agSym.getType());
                                        	        break;
					case 'I':       ret_object=new I(sym, agSym.getType());
							break;
					default:        
							break;
                        	}
                	}
        	}
        	else if (!sym.compare(0,strlen("item("),"item("))
                	ret_object=new Item(sym, agSym.getType());
		else if (!sym.compare(0,strlen(" ")," "))
			ret_object=new GenericAGSymbol(sym, agSym.getType());
		else
			errormessage="No matching class for the unexpected symbol read from the grammar file: '";
	}
	else{   //Grammar has already been made. We are using existing productions to generate a tree/phenotype. 

		if (typeid(agSym)==typeid(S))			ret_object=new S(sym, agSym.getType(), productions);
		else if (typeid(agSym)==typeid(K))		ret_object=new K(sym, agSym.getType(), productions);
		else if (typeid(agSym)==typeid(I))		ret_object=new I(sym, agSym.getType(), productions);
		else if (typeid(agSym)==typeid(Item))		ret_object=new Item(sym, agSym.getType());
		else if (typeid(agSym)==typeid(GenericAGSymbol))	ret_object=new GenericAGSymbol(sym, agSym.getType());
		else
			errormessage="Unexpected symbol read from the grammar file that could not be downcast to any of the known symbolclasses: '";
	}

	if (!ret_object){
                cerr<<errormessage<<sym<<"'"<<endl;
                exit(0);
        }	

	return ret_object;	

}

///////////////////////////////////////////////////////////////////////////////
// Destructor.
AGLookUp::~AGLookUp(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'AGLookUp::~AGLookUp()' called\n";
#endif
	
}


#endif
