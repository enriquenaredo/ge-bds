// symbolclasses.cpp -*- C++ -*-
#ifndef _SYMBOLCLASSES_CPP_
#define _SYMBOLCLASSES_CPP_

//#define DEBUG_LEVEL 2

#include <iostream>
#include <string>
#include <GE/ge.h>
#include <typeinfo>
#include "symbolclasses.h"
#include "knapsack.h"

using namespace std;


/************************************************************
// 	Helper functions and related variables.                      
*************************************************************/
const int TERMINATOR=-1;
vector<int> allowedItemsVector(KSITEMS,TERMINATOR);
int unused[KSITEMS]={TERMINATOR};

///////////////////////////////////////////////////////////////////////////////
// The following function generates a list of items that are allowed given a
// list of indices each specifying whether the corresponding items has been 
// used already or not, and their contributions towards the weights. 
const vector<int>* genAllowedItems(const vector<bool>* k_used){
#if (DEBUG_LEVEL >= 1)
        cerr << "'genAllowedItems(const vector<bool>*)' called\n";
	cerr << "k_used->size() = "<<k_used->size()<<endl;
#endif
        allowedItemsVector[0]=TERMINATOR;
        if (k_used->size()!=KSITEMS){
                cerr<<"i_usedItems vector of incorrect size: "<<k_used->size()<<". Must be: "<<KSITEMS<<endl;
                exit(0);
        }

        unsigned int weights[KSKNAPSACKS]={0};
	unsigned int uitem=0;
        for (unsigned int item=0;item<KSITEMS;item++){
                if((*k_used)[item]){
                        for (unsigned int ks=0;ks<KSKNAPSACKS;ks++)
                                weights[ks]+=KSweights[ks][item];
                }
                else{
                        unused[uitem++]=item;
                }
#if (DEBUG_LEVEL >= 1)
	cout<<"unused items: ";
	for (unsigned int w=0;w<KSITEMS;w++)
		cout<<unused[w]<<" ";
 	cout<<endl;        
#endif
        }                                                                           

	if (uitem<KSITEMS)	unused[uitem]=TERMINATOR;


#if (DEBUG_LEVEL >= 1)
	cout<<"used items: ";
	for (unsigned int w=0;w<KSITEMS;w++)
		cout<<(*k_used)[w]<<" ";
 	cout<<endl;        

	cout<<"previous weights: ";
	for (unsigned int w=0;w<KSKNAPSACKS;w++)
		cout<<weights[w]<<" ";
 	cout<<endl;        
	cout<<"unused items: ";
	for (unsigned int w=0;w<KSITEMS;w++)
		cout<<unused[w]<<" ";
 	cout<<endl;        
#endif

        unsigned int aitems=0; 
	uitem=0;
        for (uitem=0;uitem<KSITEMS && unused[uitem]!=TERMINATOR;uitem++){
                bool pass=true;
                for (unsigned int ks=0;(ks<KSKNAPSACKS)&&pass;ks++)
                        if(KSweights[ks][unused[uitem]]+weights[ks]>KSconstraints[ks])
                                pass=false;
                if(pass){
                        allowedItemsVector[aitems++]=unused[uitem];
                }
        }

	if (aitems < KSITEMS)	allowedItemsVector[aitems]=TERMINATOR;

#if (DEBUG_LEVEL >= 1)
	cout <<"\n aitems = "<<aitems<<" uitem = "<<uitem<<endl;
	cout<<"allowedItemsVector: ";
	for (unsigned int w=0;w<allowedItemsVector.size()&&allowedItemsVector[w]!=TERMINATOR;w++)
		cout<<allowedItemsVector[w]<<" ";
 	cout<<endl;        
#endif


//////////////////////////////
//Debugging code: disables forward checking
//and allows for senseless individuals. 
 
//for (uitem=0;uitem<KSITEMS;uitem++)
//	allowedItemsVector[uitem]=uitem;

//
////////////////////////////


        return &allowedItemsVector;
}


/****************************************************************************
* 			Methods for class: Item
*****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// Constructor: passes on the arguments to superclass.
Item::Item(const string newArray, const SymbolType newType):AGSymbol(newArray, newType){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::Item(const string, SymbolType)' called\n";
#endif

        //Code to initialise itemNo. 
	//Expected that item symbols are specified as: item(n);
	//where n is an integer and 0<=n<KSITEMS. 
	int numlength = strlen(newArray.c_str())-strlen("item();");
        char current;
        itemNo=0;
        int pointer=strlen("item(");
	if ( newArray[pointer]=='e')
		itemNo=KSENDITEM;
	else{ 
		for (int count=0;count<numlength;count++){
                	current=newArray[pointer++];
                	if (current>='0' && current <='9')
                        	itemNo=itemNo*10+current-'0';
                	else{
                        	cerr<<"Misspelling in Grammar file: '"<<newArray<<"'. Each item should be written as: item(n);"<<endl;
                        	exit(0);
                	}
        	}
		if (itemNo>=KSITEMS){
			cerr<<"Out of range itemNo: '"<<itemNo<<"' for '"<<newArray<<"'. Maximum is: "<<(KSITEMS-1)<<endl;
			exit(0);
		}
	}

}

///////////////////////////////////////////////////////////////////////////////
// Copy constructor.
Item::Item(const Item &copy):AGSymbol(copy){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::Item(const Item&)' called\n";
#endif
        itemNo=copy.getItemNo();
}


///////////////////////////////////////////////////////////////////////////////
// Destructor.
Item::~Item(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::~Item()' called\n";
#endif
}

///////////////////////////////////////////////////////////////////////////////
//Corresponds to the rule I::=item0(); | ... | itemn();
AGMapState Item::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::updateInheritedAttributes(AGContext &, const int, AGLookUp&)' called\n";
#endif

	AGSymbol* data = context.getParent()->getData();
        if (typeid(*data)!=typeid(I)){
		cerr<<"error: in call Item::updateInheritedAttributes(..arguments..): can not cast to 'I*'"<<endl;
		exit(1);
        }
    
	I* i = static_cast<I*>(data);
	const vector<bool>* a_items = i->getValidProductionIndices();
	//If the parent node does not allow this item, it is a constraint violation; it should not happen 
	//when forward checking is in use. 
	if (a_items!=NULL)
		if (!(*a_items)[itemNo])
			return CONSTRAINT_VIOLATION;

	//else: write code to check constraint violation in the absence of forward checking. 

	return PASS;
}

///////////////////////////////////////////////////////////////////////////////
// A default implementation; only returns success.
AGMapState Item::updateSynthesisedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::updateSynthesisedAttributes(AGContext &context, const int, AGLookUp&)' called\n";
#endif
	return PASS;
}

///////////////////////////////////////////////////////////////////////////////
// Returns a 0 indexed item number for this object. This helps to identify the 
// item that this object encapsulates. 
unsigned int Item::getItemNo() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::getItemNo()' called\n";
#endif
        return itemNo;
}

///////////////////////////////////////////////////////////////////////////////
// Copy newValue.
Item &Item::operator=(const Item newValue){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item& Item::operator=(const Item)' called\n";
#endif

        //Pass it on to the superclass.
        AGSymbol::operator=(newValue);
        itemNo = newValue.getItemNo();
        return *this;
}


/******************
Not Needed
//////////////////////////////////////////////////////////////////////////////
// Item comparison operator.
bool Item::operator==(const Item &newSymbol){
#if (DEBUG_LEVEL >= 2)
        cerr << "'bool AGSymbol::operator==(const AGSymbol&)' called\n";
#endif

        return (AGSymbol::operator==(newSymbol) && newSymbol.getItemNo()==getItemNo());
}
******************/


/****************************************************************************
* 			Methods for class: I
*****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// Constructor: passes on the arguments to superclass. rulePtr points to the 
// Rule object for the non-terminal <I>.
I::I(const string newArray, const SymbolType newType, const unsigned int productions):AGSymbol(newArray, newType){
#if (DEBUG_LEVEL >= 2)
        cerr << "'I::I(const string, SymbolType, const unsigned int)' called\n";
#endif
        //add code to initialise:
	s_usedItemNo=-1;
	if (productions>0){
		if (productions==KSITEMS+1){			
			i_validProductionIndices.resize(productions,false);
			i_validProductionIndices[KSITEMS]=true; //The ending item is always eligible
		}
		else{
			cerr<<"Grammar mismatches with problem specification:  KSITEMS+1="<<(KSITEMS+1)<<" #items in grammar="<<productions<<endl;
			exit(0);  
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
// Copy constructor.
I::I(const I &copy):AGSymbol(copy){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::Item(const Item&)' called\n";
#endif
        s_usedItemNo=copy.getUsedItemNo();
	const vector<bool>* v_prods=copy.getValidProductionIndices();
	unsigned int size=v_prods->size();
	if (i_validProductionIndices.size()!=size)
		i_validProductionIndices.resize(size);
	for (unsigned int i=0;i<size;i++)
		i_validProductionIndices[i]=(*v_prods)[i];
}


///////////////////////////////////////////////////////////////////////////////
// Destructor.
I::~I(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'I::~I()' called\n";
#endif
}


///////////////////////////////////////////////////////////////////////////////
//This pertains to productions <K>::= <I> | <I><K>. In both these cases the 
//inherited attributes of <I> only depend upon the parent node of type <K>.
AGMapState I::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'I::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp)' called\n";
#endif
	//Add code to update i_validProductionIndices from the usedItems list
	//of the parent Node which should be of type 'K'

	AGSymbol* data = context.getParent()->getData();
	if (typeid(*data)!=typeid(K)){
	        //If the cast fails, then it is an error. Check the grammar.
                cerr<<"error: in call I::updateInheritedAttributes(..arguments..): can not cast to 'K*'"<<endl;
                exit(1);
        }
	//If forward checking is enabled
	if (getValidProductionIndices()){
		const vector<int>* allowedItems;
	        K* k =  static_cast<K*>(data);
		allowedItems=genAllowedItems(k->getUsedItems());

		unsigned int a_size = allowedItems->size();
		for (unsigned int i=0;i<a_size && (*allowedItems)[i]!=TERMINATOR;i++)
			i_validProductionIndices[(*allowedItems)[i]]=true;
		//Always true.
		i_validProductionIndices[KSITEMS]=true;
	}

	return PASS;
}


///////////////////////////////////////////////////////////////////////////////
// This pertains to productions I::=item(0); | .... | item(n); | item(e);
AGMapState I::updateSynthesisedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'Item::updateAttributes(AGContext &context)' called\n";
#endif
	AGSymbol* data = context.getStartIndex()->getData();
	if (typeid(*data)!=typeid(Item)){
	//If the cast fails, then it is an error. Check the grammar.
		cerr<<"error: in call I::updateSynthesisedAttributes(..arguments..): can not cast to Item*"<<endl;
		exit(1);
	}

	Item* i =  static_cast<Item*>(data);
	s_usedItemNo=i->getItemNo();

	
	//If the selected item was not allowed, it is a constraint violation. 
	//It should not happen though if (a): forward checking is correctly
	//implemented, or (b): if the check is also made (which it is) in the 
	//Item class.
	if (!i_validProductionIndices[s_usedItemNo])
		return CONSTRAINT_VIOLATION;

	return PASS;
}


///////////////////////////////////////////////////////////////////////////////
// Returns a 0 indexed item number for this object. This helps to identify the 
// item that this object encapsulates. 
const unsigned int I::getUsedItemNo() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'I::getUsedItemNo() const' called\n";
#endif
        return s_usedItemNo;
}

const vector<bool>* I::getValidProductionIndices() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'I::getValidProductionIndices()' called\n";
#endif
	return &i_validProductionIndices;
}


///////////////////////////////////////////////////////////////////////////////
// Copy newValue.
I& I::operator=(const I newValue){
#if (DEBUG_LEVEL >= 2)
        cerr << "'I& I::operator=(const I)' called\n";
#endif

        //Pass it on to the superclass.
        AGSymbol::operator=(newValue);
        s_usedItemNo = newValue.getUsedItemNo();
	const vector<bool>* a_items=newValue.getValidProductionIndices();
	i_validProductionIndices.resize(a_items->size());	
	for (unsigned int c=0;c<a_items->size();c++)
		i_validProductionIndices[c]=(*a_items)[c];
	i_validProductionIndices[KSITEMS]=true; //always true.
        return *this;
}


/********
Not Needed
//////////////////////////////////////////////////////////////////////////////
// Item comparison operator.
bool I::operator==(const I &newSymbol){
#if (DEBUG_LEVEL >= 2)
        cerr << "'bool I::operator==(const I&)' called\n";
#endif

        bool retval= ((AGSymbol::operator==(newSymbol)) && newSymbol.getUsedItemNo()==getUsedItemNo());
	if (retval){
			const vector<bool>* a_items=newSymbol.getValidProductionIndices();
		 	for (unsigned int c=0;c<KSITEMS&&retval;c++){
				retval=retval&&(a_items[c]&i_validProductionIndices[c]);	
			}
	}
	return retval;
}
*******/


/****************************************************************************
* 			Methods for class: K
*****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// Constructor: passes on the arguments to superclass. rulePtr points to the  
// Rule object for the non-terminal <K>.
K::K(const string newArray, const SymbolType newType, const unsigned int productions):AGSymbol(newArray, newType){
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::K(const string, SymbolType, const unsigned int)' called\n";
#endif
        //add code to initialise:
	if (productions>0){
		i_validProductionIndices.resize(productions,false);
	}
	i_usedItems.resize(KSITEMS,false);
}

///////////////////////////////////////////////////////////////////////////////
// Copy constructor.
K::K(const K &copy):AGSymbol(copy){
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::K(const K&)' called\n";
#endif
        const vector<bool>* v_prods=copy.getValidProductionIndices();
        unsigned int size=v_prods->size();
        if (i_validProductionIndices.size()!=size)
                i_validProductionIndices.resize(size);
        for (unsigned int i=0;i<size;i++)
                i_validProductionIndices[i]=(*v_prods)[i];
	//reusing the same vector to copy another: i_usedItems;
        v_prods=copy.getUsedItems();
        for (unsigned i=0;i<v_prods->size();i++)
                i_usedItems[i]=(*v_prods)[i];
}


///////////////////////////////////////////////////////////////////////////////
// Destructor.
K::~K(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::~K()' called\n";
#endif
}


///////////////////////////////////////////////////////////////////////////////
// This pertains to productions:
//	 <S>::= <K>
// 	 <K>::= <I><K>. 
// <K> at the right side, depends on both <I> and, <K> on the left side. 
AGMapState K::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp)' called\n";
#endif
	AGSymbol* data=context.getParent()->getData();
	const vector<int>* allowedItems=NULL;

	if (typeid(*data)==typeid(S)){
		//i_usedItems stays initialised to false.
		//but initialise i_validProductionIndices appropriately. 
		//If forward checking is enabled
		if (getValidProductionIndices()){
                	allowedItems=genAllowedItems(getUsedItems());
		}
	}
	else if(typeid(*data)==typeid(K) && typeid(*(context.getStartIndex()->getData()))==typeid(I)){
		K* k =  static_cast<K*>(data);  //parent node
		I* i =  static_cast<I*>(context.getStartIndex()->getData());  //left sibling node
		//update used items from the parent node
		const vector<bool>* uitems=k->getUsedItems();
		for (unsigned int i=0;i<KSITEMS;i++)
			i_usedItems[i]=(*uitems)[i];
		//update used items from the left sibling node
		if (i->getUsedItemNo()!=KSENDITEM){
			i_usedItems[i->getUsedItemNo()]=true;
			if (getValidProductionIndices()){
                	        allowedItems=genAllowedItems(getUsedItems());
                	}
		}
	}
	else{
                //If the cast fails, then it is an error. Check the grammar.
                cerr<<"error: in call K::updateInheritedAttributes(..arguments..): can not downcast appropriately'"<<endl;
		cerr<<"Parent = "<< (*data)<<" : left sibling (startIndex->getData()) = "<<(*(context.getStartIndex()->getData()))<<endl;
                exit(1);
        }
	
	//There are two rules: <K>::=<I> | <I><K>. We only want to allow the second 
	//(recursive) rule if more than one items are eligible for selection.
	if(allowedItems){
		for (unsigned int i=0;i<allowedItems->size() && (i<2) && (*allowedItems)[i]!=TERMINATOR;i++)
			i_validProductionIndices[i]=true;
	}		
	 i_validProductionIndices[0]=true; //the non-recursive option is true in any case. 

	return PASS;
}

///////////////////////////////////////////////////////////////////////////////
// This pertains to productions <K>::=<I> | <I><K>. However, nothing needs to be
// done, when forward checking is in place. 
AGMapState K::updateSynthesisedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::updateSynthesisedAttributes(AGContext &, const int, AGLookUp&)' called\n";
#endif
	//If forward checking is not in place, add code to check for constraint violations
	//as the items encoded below might be invalid.

	return PASS;
}


///////////////////////////////////////////////////////////////////////////////
// Facilitates forward checking. 
const vector<bool>* K::getValidProductionIndices() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::getValidProductionIndices() const' called\n";
#endif
	return &i_validProductionIndices;
}

//////////////////////////////////////////////////////////////////////////////
// Helps determining the value of valid productions for forward checking.
const vector<bool>* K::getUsedItems() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'K::getUsedItems() const' called\n";
#endif
        return &i_usedItems;
}


///////////////////////////////////////////////////////////////////////////////
// Copy newValue.
K& K::operator=(const K newValue){
#if (DEBUG_LEVEL >= 2)
        cerr << "'K& K::operator=(const K)' called\n";
#endif

        //Pass it on to the superclass.
        AGSymbol::operator=(newValue);
	const vector<bool>* productions=newValue.getValidProductionIndices();
	i_validProductionIndices.resize(productions->size());
	for (unsigned int c=0;c<productions->size();c++)
		i_validProductionIndices[c]=(*productions)[c];

	//reusing the same vector to copy another i_usedItems;
	productions=newValue.getUsedItems();
	for (unsigned c=0;c<productions->size();c++)
                i_usedItems[c]=(*productions)[c];
        return *this;

}


/****************************************************************************
* 			Methods for class: S
*****************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// Constructor: passes on the arguments to superclass. rulePtr points to the  
// Rule object for the non-terminal <S>.
S::S(const string newArray, const SymbolType newType, const unsigned int):AGSymbol(newArray, newType){
#if (DEBUG_LEVEL >= 2)
        cerr << "'S::S(const string, SymbolType, const unsigned int)' called\n";
#endif

	i_allowedItems.resize(KSITEMS);
	//Initially all items are allowed.
	for (unsigned int i=0;i<KSITEMS;i++){
		i_allowedItems[i]=true; 
	}
}

///////////////////////////////////////////////////////////////////////////////
// Copy constructor.
S::S(const S &copy):AGSymbol(copy){
#if (DEBUG_LEVEL >= 2)
        cerr << "'S::S(const S&)' called\n";
#endif
        const vector<bool>* v_prods=copy.getAllowedItems();
        unsigned int size=v_prods->size();
        if (i_allowedItems.size()!=size)
                i_allowedItems.resize(size);
        for (unsigned int i=0;i<size;i++)
                i_allowedItems[i]=(*v_prods)[i];
}


///////////////////////////////////////////////////////////////////////////////
// Destructor.
S::~S(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'S::~S()' called\n";
#endif
}


///////////////////////////////////////////////////////////////////////////////
// Empty Implementation
AGMapState S::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'S::updateInheritedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp)' called\n";
#endif
	return PASS;
}

///////////////////////////////////////////////////////////////////////////////
// Empty Implementation
AGMapState S::updateSynthesisedAttributes(AGContext &context, const int prodIndex, AGLookUp& lookUp){
#if (DEBUG_LEVEL >= 2)
        cerr << "'S::updateSynthesisedAttributes(AGContext &, const int, AGLookUp&)' called\n";
#endif
	return PASS;
}


//////////////////////////////////////////////////////////////////////////////
// Helps determining the value of valid productions for forward checking.
const vector<bool>* S::getAllowedItems() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'S::getAllowedItems() const' called\n";
#endif
        return &i_allowedItems;
}


///////////////////////////////////////////////////////////////////////////////
// Copy newValue.
S& S::operator=(const S newValue){
#if (DEBUG_LEVEL >= 2)
        cerr << "'S& S::operator=(const S)' called\n";
#endif

        //Pass it on to the superclass.
        AGSymbol::operator=(newValue);
	const vector<bool>* v_prods=newValue.getAllowedItems();
	unsigned int size=v_prods->size();
	if (i_allowedItems.size()!=size)
		i_allowedItems.resize(size);
	for (unsigned int i=0;i<size;i++)
		i_allowedItems[i]=(*v_prods)[i];

        return *this;

}





#endif
