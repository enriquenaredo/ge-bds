//GenericAGSymbol.h -*- C++ -*-
#ifndef _GENERICAGSYMBOL_H_
#define _GENERICAGSYMBOL_H_

//////////////////////////////////////////////////////////////////////////////
// This class corresponds to symbols in the grammar file which do not require unique
// attributes and specialised methods for updating attributes. Examples are
// blank space character in this case, but also opening and closes braces
// for some other problems.

#include "AGSymbol.h"

using namespace std;

class GenericAGSymbol : public AGSymbol{
        public:
                GenericAGSymbol(const string, const SymbolType);
                GenericAGSymbol(const GenericAGSymbol&);
                ~GenericAGSymbol();
                virtual AGMapState updateSynthesisedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
                virtual AGMapState updateInheritedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
                GenericAGSymbol &operator=(const GenericAGSymbol);
};

#endif
