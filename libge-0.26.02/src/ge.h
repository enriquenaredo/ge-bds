#ifndef _GE_H_
#define _GE_H_

#ifdef __cplusplus
	#include "Genotype.h"
	#include "Phenotype.h"
	#include "Mapper.h"
	#include "Tree.h"
	#include "Initialiser.h"
	#include "GEGrammar.h"
	#include "CFGrammar.h"
	#include "GEGrammarSI.h"
	#include "Rule.h"
	#include "Production.h"
	#include "Symbol.h"
        #include "AGSymbol.h"
        #include "AGLookUp.h"
        #include "AGContext.h"
	#include "AGDerivationTree.h"
	#include "GenericAGSymbol.h"
	//#ifdef GALIB_INCLUDEPATH
	//	#include "GE_MITGALIB.h"
	//#endif
#endif
#ifdef GE_ILLIGALSGA
	#include "GE_ILLIGALSGA.h"
#endif
#endif

