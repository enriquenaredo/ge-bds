// Symbol.cpp -*- C++ -*-
#ifndef _AGCONTEXT_CPP_
#define _AGCONTEXT_CPP_

//#include <config.h>

#include<iostream>

#include "AGContext.h"


///////////////////////////////////////////////////////////////////////////////
// Empty Constructor. The class member variables should be subsequently set
// using the appropriate set methods. 
AGContext::AGContext(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'AGContext::AGContext()' called\n";
#endif

}


///////////////////////////////////////////////////////////////////////////////
// Create a new AGContext Object with a pointer to parent symbol and the indices
// to start and end of the current rule in the developing phenotype.
AGContext::AGContext(AGTree* pSymbol, AGTree::iterator start, AGTree::iterator  end){
#if (DEBUG_LEVEL >= 2)
        cerr << "'AGContext::AGContext(int * pSymbol, int start, int end)' called\n";
#endif

	parent = pSymbol; 
	startIndex = start;  
	endIndex = end;
}


///////////////////////////////////////////////////////////////////////////////
// The destructor. 
AGContext::~AGContext(){
#if (DEBUG_LEVEL >= 2)
        cerr << "'AGContext::~AGContext()' called\n";
#endif

}

/////////////////////////////////////////////////////////////////////////////// 
// Gets the parent node in the derivation tree in the current context. Under the 
// currently envisioned circumstances the Symbol pointed to by the parent should 
// have the same non-terminal as in the left hand side of the just expanded Rule. 
// However, the pointer parent should point to the instance of this non-terminal 
// in the derivation tree. 
AGTree* AGContext::getParent() const{
#if (DEBUG_LEVEL >= 2)
        cerr << "'int * const AGContext::getParent() const' called\n";
#endif

	return parent; 
}

///////////////////////////////////////////////////////////////////////////////
// Sets the parent of this context. 
void AGContext::setParent(AGTree* p){
#if (DEBUG_LEVEL >= 2)
        cerr << "'void AGContext::setParent(AGTree* p)' called\n";
#endif

	parent = p;
}


///////////////////////////////////////////////////////////////////////////////
// Returns the index to the starting point of the just expanded rule in the 
// developing phenotype.
AGTree::iterator AGContext::getStartIndex() const { 
#if (DEBUG_LEVEL >= 2)
        cerr << "'int AGContext::getStartIndex() const' called\n";
#endif

	return startIndex; 
}


///////////////////////////////////////////////////////////////////////////////
// Sets the startIndex. 
void AGContext::setStartIndex(AGTree::iterator sIndex) {
#if (DEBUG_LEVEL >= 2)
        cerr << "'void AGContext::setStartIndex(int sIndex)' called\n";
#endif

	startIndex=sIndex;
}


///////////////////////////////////////////////////////////////////////////////
// Returns the index to the end point of the just expanded rule in the 
// developing phenotype.
AGTree::iterator  AGContext::getEndIndex() const{ 
#if (DEBUG_LEVEL >= 2)
        cerr << "'const int AGContext::getEndIndex()' called\n";
#endif

	return endIndex; 
}


///////////////////////////////////////////////////////////////////////////////
// Sets the endIndex. 
void AGContext::setEndIndex(AGTree::iterator eIndex) {
#if (DEBUG_LEVEL >= 2)
        cerr << "'void AGContext::setEndIndex(int eIndex)' called\n";
#endif

	endIndex=eIndex;
}


#endif
