This is libGE.info, produced by makeinfo version 6.5 from libGE.texi.

This manual is for libGE, version 0.26.01, a C++ library that implements
the Grammatical Evolution mapping process.

   Copyright (C) 2003-2006 Biocomputing-Developmental Systems Centre,
University of Limerick, Ireland.  Permission is granted to copy,
distribute and/or modify this document under the terms of the GNU Free
Documentation License, Version 1.2 or any later versions published by
the Free Software Foundation; with no Invariant Sections, no Front-Cover
Texts, and no Back-Cover Texts.  A copy of the license is included in
the section entitled "GNU Free Documentation License".


Indirect:
libGE.info-1: 663
libGE.info-2: 301296

Tag Table:
(Indirect)
Node: Top663
Node: Overview1863
Node: Purpose2062
Node: Installation4009
Node: Introduction to libGE6262
Node: Grammatical Evolution6533
Node: Example of Mapping Process12416
Node: Using libGE15404
Node: The Mapping Process17068
Node: The System Boundary19460
Node: Programming Interface20621
Node: Class Hierarchy21101
Node: Description27863
Node: Interfaces31569
Node: Genotype32569
Node: Phenotype37272
Node: Mapper40076
Node: Rule43989
Node: Production46294
Node: Symbol48725
Node: Grammar51063
Node: Tree54331
Node: CFGrammar57472
Node: GEGrammar62141
Node: Initialiser66160
Node: GEGrammarSI68451
Node: Designing your Own Mappers73697
Node: Grammars75290
Node: Format of Grammars76015
Node: Type of Grammars81647
Node: libGE Extensions87192
Node: Examples of Grammars89571
Node: Search Engines92387
Node: IlliGAL sga-c93131
Node: GALib97924
Node: EO102437
Node: Using your own Search Engine107612
Node: Evaluators108435
Node: GCC109331
Node: S-Lang113541
Node: TinyCC117696
Node: Lua121613
Node: Using your own Evaluator127399
Node: Examples127834
Node: Santa Fe Ant Trail Problem128493
Node: Santa Fe Grammar File129921
Node: Ant Trail130246
Node: Santa Fe Implementation with GE_ILLIGALSGA130553
Node: EXAMPLES/SantaFeAntTrail/GE_ILLIGALSGA/app.c131343
Node: EXAMPLES/SantaFeAntTrail/GE_ILLIGALSGA/Makefile132977
Node: Santa Fe Implementation with GE_MITGALIB133629
Node: GALib Example User Guide134936
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/main.cpp137303
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/initfunc.cpp140206
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/GEListGenome.h141402
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-gcc.cpp142476
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-slang.cpp144537
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-tcc.cpp146739
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/santafe-lua.cpp148906
Node: EXAMPLES/SantaFeAntTrail/GE_MITGALIB/Makefile151391
Node: Santa Fe Implementation with GE_EO152205
Node: EO Example User Guide153339
Node: EXAMPLES/SantaFeAntTrail/GE_EO/main.cpp155809
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGE.h157826
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEInit.h158491
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEMutation.h159432
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEQuadCrossover.h159983
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncGCC.h160905
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncSlang.h162613
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncTCC.h164462
Node: EXAMPLES/SantaFeAntTrail/GE_EO/eoGEEvalFuncLua.h166312
Node: EXAMPLES/SantaFeAntTrail/GE_EO/Makefile168437
Node: Santa Fe Ant Trail Performance169460
Node: GALib performance on Santa Fe Ant Trail171172
Node: EO performance on Santa Fe Ant Trail171609
Node: Cart Centering Problem172034
Node: Cart Centering Optimal Solution176346
Node: Cart Centering Grammar177334
Node: EXAMPLES/CartCentering/GEcart.c178554
Node: EXAMPLES/CartCentering/cartcenterstart.c183064
Node: EXAMPLES/CartCentering/cartcenterend.c184254
Node: EXAMPLES/CartCentering/WrappedCPhenotype185265
Node: Cart Centering Implementation with GE_ILLIGALSGA186024
Node: EXAMPLES/CartCentering/GE_ILLIGALSGA/app.c186829
Node: EXAMPLES/CartCentering/GE_ILLIGALSGA/Makefile191171
Node: Cart Centering Implementation with GE_MITGALIB191830
Node: Cart Centering GALib Example User Guide193655
Node: EXAMPLES/CartCentering/GE_MITGALIB/main.cpp196200
Node: EXAMPLES/CartCentering/GE_MITGALIB/initfunc.cpp201757
Node: EXAMPLES/CartCentering/GE_MITGALIB/GEListGenome.h202937
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-gcc.cpp204196
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-slang.cpp209274
Node: EXAMPLES/CartCentering/GE_MITGALIB/GEcart.sl213651
Node: EXAMPLES/CartCentering/GE_MITGALIB/SLangStartCode214401
Node: EXAMPLES/CartCentering/GE_MITGALIB/SLangEndCode215712
Node: EXAMPLES/CartCentering/GE_MITGALIB/WrappedSlangPhenotype217055
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-libtcc.cpp217751
Node: EXAMPLES/CartCentering/GE_MITGALIB/cartcenter-lua.cpp225213
Node: EXAMPLES/CartCentering/GE_MITGALIB/GEcart.lua230908
Node: EXAMPLES/CartCentering/GE_MITGALIB/LuaStartCode231648
Node: EXAMPLES/CartCentering/GE_MITGALIB/LuaEndCode232566
Node: EXAMPLES/CartCentering/GE_MITGALIB/WrappedLuaPhenotype233742
Node: EXAMPLES/CartCentering/GE_MITGALIB/Makefile234365
Node: Cart Centering Implementation with GE_EO235307
Node: Cart Centering EO Example User Guide236570
Node: EXAMPLES/CartCentering/GE_EO/main.cpp238815
Node: EXAMPLES/CartCentering/GE_EO/eoGE.h242538
Node: EXAMPLES/CartCentering/GE_EO/eoGEInit.h243262
Node: EXAMPLES/CartCentering/GE_EO/eoGEMutation.h244219
Node: EXAMPLES/CartCentering/GE_EO/eoGEQuadCrossover.h244778
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-gcc_tcc.h245726
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-slang.h250722
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-libtcc.h255098
Node: EXAMPLES/CartCentering/GE_EO/eoGEEvalFunc-lua.h262069
Node: EXAMPLES/CartCentering/GE_EO/Makefile267760
Node: Cart Centering Performance268908
Node: GALib performance on Cart Centering270514
Node: EO performance on Cart Centering270826
Node: Intertwined Spirals Problem271129
Node: Intertwined Spirals Grammar272422
Node: GEspiral.c273027
Node: spiralstart.c274935
Node: spiralend.c276248
Node: WrappedCPhenotype277065
Node: Intertwined Spirals Implementation with GE_MITGALIB277662
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/main.cpp281144
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/initfunc.cpp285437
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/GEListGenome.h286637
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-gcc.cpp287917
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-slang.cpp292790
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/GEspiral.sl297026
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/SLangStartCode297806
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/SLangEndCode299071
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/WrappedSLangPhenotype299929
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-libtcc.cpp301296
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/spiral-lua.cpp308134
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/GEspiral.lua313695
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/LuaStartCode314473
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/LuaEndCode315554
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/WrappedLuaPhenotype316446
Node: EXAMPLES/IntertwinedSpirals/GE_MITGALIB/Makefile317097
Node: Intertwined Spirals Implementation with GE_EO318051
Node: Intertwined Spirals EO Example User Guide319081
Node: EXAMPLES/IntertwinedSpirals/GE_EO/GEEA.cpp321395
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGE.h325153
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEInit.h325897
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEMutation.h326874
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEQuadCrossover.h327453
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-gcc_tcc.h328421
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-slang.h333091
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-libtcc.h337266
Node: EXAMPLES/IntertwinedSpirals/GE_EO/eoGEEvalFunc-lua.h343914
Node: EXAMPLES/IntertwinedSpirals/GE_EO/Makefile349321
Node: Intertwined Spirals Performance350486
Node: GALib performance on Intertwined Spirals352139
Node: EO performance on Intertwined Spirals352464
Node: FAQ352780
Node: Copying This Manual355379
Node: References376411
Node: Index377379

End Tag Table
